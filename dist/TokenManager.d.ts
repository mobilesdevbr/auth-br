import { Token, Account } from '.';
declare abstract class TokenManager {
    abstract sign(account: Account): Promise<Token | null>;
    abstract verify(token: string): Promise<boolean | any>;
}
export default TokenManager;
