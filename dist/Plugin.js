"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fastify_plugin_1 = __importDefault(require("fastify-plugin"));
const fastify_sensible_1 = __importDefault(require("fastify-sensible"));
const Configuration_1 = __importDefault(require("./Configuration"));
const Route_1 = __importDefault(require("./Route"));
async function Plugin(fastify, options) {
    fastify.register(fastify_sensible_1.default);
    const configuration = new Configuration_1.default();
    configuration.validate(options);
    configuration.driver.initialize();
    const route = new Route_1.default(configuration);
    route.initialize(fastify, route);
}
const FastifyPlugin = fastify_plugin_1.default(Plugin);
exports.default = FastifyPlugin;
