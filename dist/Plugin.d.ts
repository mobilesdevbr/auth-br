/// <reference types="node" />
/// <reference types="fastify" />
/// <reference types="fastify-sensible" />
import DriverManager from './DriverManager';
import TokenManager from './TokenManager';
import * as http from 'http';
export interface PluginOptions<N> {
    driver: DriverManager<N>;
    token: TokenManager;
    config: N;
}
declare const FastifyPlugin: (instance: import("fastify").FastifyInstance<http.Server, http.IncomingMessage, http.ServerResponse>, options: PluginOptions<any>, callback: (err?: import("fastify").FastifyError) => void) => void;
export default FastifyPlugin;
