"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthorizeOptions = {
    schema: {
        description: 'Authorization endpoint',
        tags: ['account', 'token'],
        body: {
            type: 'object',
            properties: {
                email: {
                    type: 'string',
                    format: 'email',
                    maxLength: 256
                },
                password: {
                    type: 'string',
                    minLength: 6,
                    maxLength: 32
                }
            },
            required: [
                'email', 'password'
            ]
        },
        response: {
            '2xx': {
                type: 'object',
                properties: {
                    token: {
                        description: 'Access token',
                        type: 'string'
                    },
                    expire: {
                        description: 'Token expiration em seconds',
                        type: 'integer'
                    }
                },
                required: [
                    'token', 'expire'
                ]
            },
            '4xx': {
                type: 'object',
                properties: {
                    statusCode: {
                        type: 'integer'
                    },
                    error: {
                        type: 'string'
                    },
                    message: {
                        type: 'string'
                    }
                },
                required: [
                    'statusCode', 'error', 'message'
                ]
            },
            '5xx': {
                type: 'object',
                properties: {
                    statusCode: {
                        type: 'integer'
                    },
                    error: {
                        type: 'string'
                    },
                    message: {
                        type: 'string'
                    }
                },
                required: [
                    'statusCode', 'error', 'message'
                ]
            }
        }
    }
};
