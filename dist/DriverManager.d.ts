import { Account } from '.';
declare abstract class DriverManager<N> {
    abstract register(account: Account, extra: any): Promise<boolean>;
    abstract find(email: string): Promise<Account | null>;
    abstract initialize(): Promise<void>;
    abstract validate(options: N): Promise<void>;
}
export default DriverManager;
