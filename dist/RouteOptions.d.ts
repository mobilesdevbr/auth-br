export declare const AuthorizeOptions: {
    schema: {
        description: string;
        tags: string[];
        body: {
            type: string;
            properties: {
                email: {
                    type: string;
                    format: string;
                    maxLength: number;
                };
                password: {
                    type: string;
                    minLength: number;
                    maxLength: number;
                };
            };
            required: string[];
        };
        response: {
            '2xx': {
                type: string;
                properties: {
                    token: {
                        description: string;
                        type: string;
                    };
                    expire: {
                        description: string;
                        type: string;
                    };
                };
                required: string[];
            };
            '4xx': {
                type: string;
                properties: {
                    statusCode: {
                        type: string;
                    };
                    error: {
                        type: string;
                    };
                    message: {
                        type: string;
                    };
                };
                required: string[];
            };
            '5xx': {
                type: string;
                properties: {
                    statusCode: {
                        type: string;
                    };
                    error: {
                        type: string;
                    };
                    message: {
                        type: string;
                    };
                };
                required: string[];
            };
        };
    };
};
