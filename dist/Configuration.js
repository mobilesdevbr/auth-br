"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Exception_1 = __importDefault(require("./lib/Exception"));
const DriverManager_1 = __importDefault(require("./DriverManager"));
const TokenManager_1 = __importDefault(require("./TokenManager"));
class Configuration {
    validate(options) {
        if (options.driver == null) {
            throw new Exception_1.default('driver == null');
        }
        if (!(options.driver instanceof DriverManager_1.default)) {
            throw new Exception_1.default('driver != DriverManager');
        }
        if (options.token == null) {
            throw new Exception_1.default('token == null');
        }
        if (!(options.token instanceof TokenManager_1.default)) {
            throw new Exception_1.default('token != TokenManager');
        }
        this.driver = options.driver;
        this.token = options.token;
        this.driver.validate(options.config);
    }
}
exports.default = Configuration;
