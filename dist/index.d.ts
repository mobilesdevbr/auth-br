import Plugin from './Plugin';
import _DriverManager from './DriverManager';
import _TokenManager from './TokenManager';
import _Exception from './lib/Exception';
export declare const Exception: typeof _Exception;
export declare const DriverManager: typeof _DriverManager;
export declare const TokenManager: typeof _TokenManager;
export interface Account {
    id: string;
    name?: string;
    email?: string;
    password?: string;
}
export interface Token {
    token: string;
    expire: number;
}
export default Plugin;
