"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const RouteOptions_1 = require("./RouteOptions");
class Route {
    constructor(config) {
        this.config = config;
    }
    async initialize(fastify, route) {
        fastify.post('/account/authorize', RouteOptions_1.AuthorizeOptions, async function (req, reply) {
            return await route.authorize(req, reply);
        });
    }
    async authorize(req, reply) {
        try {
            const account = await this.config.driver.find(req.body.email);
            if (!account) {
                reply.unauthorized();
                return reply;
            }
            if (!await bcrypt_1.default.compare(req.body.password, account.password)) {
                reply.unauthorized();
                return reply;
            }
            const token = await this.config.token.sign(account);
            if (!token) {
                reply.unauthorized();
                return reply;
            }
            reply.send(token);
            return reply;
        }
        catch (e) {
            reply.internalServerError();
            return reply;
        }
    }
}
exports.default = Route;
