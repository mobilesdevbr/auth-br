import DriverManager from './DriverManager';
import TokenManager from './TokenManager';
declare class Configuration<N> {
    driver: DriverManager<N>;
    token: TokenManager;
    validate(options: any): void;
}
export default Configuration;
