import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import Configuration from './Configuration';
declare class Route {
    private config;
    constructor(config: Configuration<any>);
    initialize(fastify: FastifyInstance, route: Route): Promise<void>;
    authorize(req: FastifyRequest, reply: FastifyReply<any>): Promise<any>;
}
export default Route;
