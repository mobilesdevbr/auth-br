import Exception from './lib/Exception'
import DriverManager from './DriverManager'
import TokenManager from './TokenManager'

/**
 * Configuração geral do plugin
 */
class Configuration<N> {
  /**
   * Instância do driver, que é responsável por armazenar
   * e gerenciar os usuário
   */
  public driver: DriverManager<N>;

  /**
   * Responsável por gerar e validar o token de acesso
   */
  public token: TokenManager;

  /**
   * Valida as configurações, caso possuir erro, emitir Exception
   *
   * @param options
   */
  public validate (options) {
    if (options.driver == null) {
      throw new Exception('driver == null')
    }
    if (!(options.driver instanceof DriverManager)) {
      throw new Exception('driver != DriverManager')
    }
    if (options.token == null) {
      throw new Exception('token == null')
    }
    if (!(options.token instanceof TokenManager)) {
      throw new Exception('token != TokenManager')
    }
    this.driver = options.driver
    this.token = options.token
    //
    // Valida as opções do driver em conjunto
    //
    this.driver.validate(options.config)
  }
}

export default Configuration
