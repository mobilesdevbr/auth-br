/**
 * Exception genérico para melhorar a visibilidade do código.
 */
class Exception extends Error {
  /**
   * Exception
   *
   * @param message Mensagem de erro
   */
  constructor (message) {
    super(message)
    this.message = message
  }
}

export default Exception
