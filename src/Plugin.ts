import fp from 'fastify-plugin'
import fs from 'fastify-sensible'

import Configuration from './Configuration'
import DriverManager from './DriverManager'
import TokenManager from './TokenManager'

import Route from './Route'

import * as http from 'http'

export interface PluginOptions<N> {
  driver: DriverManager<N>
  token: TokenManager
  config: N
}

async function Plugin<T, N> (fastify, options) {
  fastify.register(fs)

  const configuration = new Configuration<N>()
  configuration.validate(options)
  configuration.driver.initialize()

  const route = new Route(configuration)
  route.initialize(fastify, route)
}

const FastifyPlugin = fp<http.Server, http.IncomingMessage, http.ServerResponse, PluginOptions<any>>(Plugin)
export default FastifyPlugin
