import { Account } from '.'

/**
 * Implementações do driver, ou seja onde será armazenado e consultado os usuários.
 */
abstract class DriverManager<N> {
    /**
     * Registra a conta do usuário
     *
     * @param account Conta do usuário definido em account.ts
     * @param extra Dados extra
     */
    abstract async register(account: Account, extra: any): Promise<boolean>;

    /**
     * Busca o usuário pelo email
     *
     * @param email, email filtrado do usuário
     */
    abstract async find(email: string): Promise<Account | null>;

    /**
     * Inicializa o driver após validar as opções
     * Importante: Definir a opção field para extender a tabela do usuário
     */
    abstract initialize(): Promise<void>;

    /**
     * Valida as opções passada no plugin
     *
     * @param {N} options Opções definidas
     */
    abstract validate(options: N): Promise<void>;
}

export default DriverManager
