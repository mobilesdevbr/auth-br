import Plugin from './Plugin'

import _DriverManager from './DriverManager'
import _TokenManager from './TokenManager'
import _Exception from './lib/Exception'

export const Exception = _Exception
export const DriverManager = _DriverManager
export const TokenManager = _TokenManager

/**
 * Definição da conta do usuário
 */
export interface Account {
  id: string;
  name?: string;
  email?: string;
  password?: string;
}

/**
 * Emissão do token
 */
export interface Token {
  token: string;
  expire: number;
}

export default Plugin
