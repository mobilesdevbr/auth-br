import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import bcrypt from 'bcrypt'

import Configuration from './Configuration'
import { AuthorizeOptions } from './RouteOptions'

class Route {
    private config: Configuration<any>

    constructor (config: Configuration<any>) {
      this.config = config
    }

    public async initialize (fastify: FastifyInstance, route: Route) {
      fastify.post('/account/authorize', AuthorizeOptions, async function (req, reply) {
        return await route.authorize(req, reply)
      })
    }

    public async authorize (req: FastifyRequest, reply: FastifyReply<any>): Promise<any> {
      try {
        const account = await this.config.driver.find(req.body.email)
        if (!account) {
          reply.unauthorized()
          return reply
        }
        if (!await bcrypt.compare(req.body.password, account.password)) {
          reply.unauthorized()
          return reply
        }
        const token = await this.config.token.sign(account)
        if (!token) {
          reply.unauthorized()
          return reply
        }
        reply.send(token)
        return reply
      } catch (e) {
        reply.internalServerError()
        return reply
      }
    }
}

export default Route
