import { Token, Account } from '.'

/**
 * Gerenciador dos tokens do usuário
 */
abstract class TokenManager {
    /**
     * Gerar token com base no usuário
     * - Não salvar dados sensíveis no token
     *
     * @param account Conta do usuário
     */
    abstract async sign(account: Account): Promise<Token | null>;

    /**
     * Verificar token do usuário
     *
     * @param {string} token Token passado pelo usuário, não há filtros.
     */
    abstract async verify(token: string): Promise<boolean | any>;
}

export default TokenManager
